<?php

namespace App\Http\Controllers;

use App\form;
use Illuminate\Http\Request;

class   FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        var_dump($request->country);
//        dd($request['country']);
//        collect($request)->except('directive')->all()
//            dd($request->all());
        $user = form::create([
            'name' => $request->name,
            'password' => $request->password,
            'file' => $request->file,
            'car' => $request->car,
            'country' => implode(',', array_values($request['country'])),
            'radio' => $request->radio,
            'check' => implode(',', array_values($request['check'])),
            'prepend' => $request->prepend,
            'append' => $request->append,
            'input2' => $request->input2,
            'textarea' => $request->textarea,
            'checkinput' => $request->checkinput,

        ]);
        return view('form');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\form $form
     * @return \Illuminate\Http\Response
     */
    public function show(form $form)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\form $form
     * @return \Illuminate\Http\Response
     */
    public function edit(form $form)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\form $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, form $form)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\form $form
     * @return \Illuminate\Http\Response
     */
    public function destroy(form $form)
    {
        //
    }
}
