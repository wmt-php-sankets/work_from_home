<?php

namespace App\Http\Controllers;
use App\Country;
use App\State;
use Illuminate\Http\Request;

class MainController extends Controller
{
            public function index(){

                $countries=Country::all()->pluck('name','id');
                return view('welcome',compact('countries'));
            }

            public function getstates($id){
                $states =State::where('country_id',$id)->pluck('name','id')->get();
                return json_encode($states);
            }
}
