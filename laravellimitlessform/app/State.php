<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table=['states'];
    protected $guarded=[];

    public function country()
    {
        return $this->belongsTo(Country::class,'countries_id');
    }
}
