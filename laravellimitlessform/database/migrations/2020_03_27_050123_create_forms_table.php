<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('password');
            $table->string('file');
            $table->string('car');
            $table->string( 'country')->toArray();
            $table->string('radio');
            $table->string('check');
            $table->string('prepend');
            $table->string('append');
            $table->string('input2');
            $table->string('textarea');
            $table->string('checkinput');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
