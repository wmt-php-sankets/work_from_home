@extends('master')
@section('js')

    <script src="{{asset('assets/js/plugins/forms/validation/validate.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/inputs/touchspin.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_inputs.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_validation.js')}}"></script>

    <script src="{{asset('assets/js/plugins/forms/inputs/inputmask.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/inputs/autosize.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/inputs/formatter.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/inputs/typeahead/handlebars.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/inputs/passy.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/inputs/maxlength.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_controls_extended.js')}}"></script>

    <script src="{{asset('assets/js/plugins/notifications/pnotify.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_multiselect.js')}}"></script>

    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radio.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_input_groups.js')}}"></script>
@endsection
@section('form')
    <div class="container-fluid">
        <form action="{{ route('form.store') }}" method="POST" id="myform" name="form">
        @csrf
        <!-- Basic text input -->
            <div class="form-group row">
                <label class="col-form-label col-lg-2">text input <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="name" class="form-control" required placeholder="Text input validation">
                </div>
            </div>
            <!-- /basic text input -->

            <!-- Basic text Password -->
            <div class="form-group row">
                <label class="col-form-label col-lg-2">text password <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <div class="form-group">
                        <div class="badge-indicator-absolute">
                            <input type="text" name="password" class="form-control" required
                                   placeholder="Enter your password">
                            <span class="badge password-indicator-badge-absolute"></span>
                        </div>
                        <span>
                <button type="button"
                        class="btn btn-info generate-badge-absolute">Generate 10 characters password</button>
                </span>
                    </div>
                </div>
            </div>
            <!-- password  input -->

            <!-- file button-->
            <div class="form-group row">
                <label class="col-form-label  col-lg-2" required> Choose File </label>
                          <div class="custom-file">
          <div class="col-lg-10">
                    <div class="custom-file">
                        <input type="file" name="file" required class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
            </div>
            <!-- /file button end-->

            <!-- Select Basic  -->
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Select Option</label>
                <div class="col-lg-10">
                    <select class="form-control form-control-uniform" name="car">
                        <option value="car1">car1</option>
                        <option value="car2">car2</option>
                        <option value="car3">car3</option>
                        <option value="car4">car4</option>
                        <option value="car5">car5</option>
                    </select>
                </div>
            </div>
            <!-- Select Basic End -->

            <!-- Multiple Select-->
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Multiple select</label>
                <div class="col-lg-10">
                    <select multiple="multiple" name="country[]" class="form-control">
                        <option selected value="">Amsterdam</option>
                        <option selected>Atlanta</option>
                        <option>Baltimore</option>
                        <option>Boston</option>
                        <option>Minneapolis</option>
                    </select>
                </div>
            </div>
            <!-- Multiple Select End-->

            <!-- Multiple Radio Button start -->
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Radio Button</label>
                <div class="col-lg-10">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input-styled" name="radio" value="male" checked
                                   data-fouc>
                            Male
                        </label>
                    </div>

                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input-styled" name="radio" value="female" data-fouc>
                            Female
                        </label>
                    </div>
                </div>
            </div>
            <!--Multiple Radio Button End  -->

            <!--Multiple Checkboxs Start -->

            <div class="form-group row">
                <label class="col-form-label col-lg-2">Check Box</label>
                <div class="custom-control custom-checkbox custom-control-inline">
                    <input type="checkbox" class="custom-control-input" name="check[]" value="car"
                           id="custom_checkbox_inline_unchecked" checked>
                    <label class="custom-control-label" for="custom_checkbox_inline_unchecked">car</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                    <input type="checkbox" class="custom-control-input" name="check[]" value="money"
                           id="custom_checkbox_inline_checked">
                    <label class="custom-control-label" for="custom_checkbox_inline_checked">Money</label>
                </div>
            </div>
            <!--Multiple Checkboxs End -->

            <!--Prepended Text Start-->

            <div class="form-group row">
                <label class="col-form-label col-lg-2">Prepended Text</label>
                <div class="col-lg-10">
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text">Prepend</span>
                        </span>
                        <input type="text" class="form-control" name="prepend" required placeholder="Placeholder">
                    </div>
                </div>
            </div>
            <!--Prepended Text End-->

            <!--Appended Text Start-->
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Appended Text</label>
                <div class="col-lg-10">
                    <div class="input-group">
                        <input type="text" name="append" class="form-control" placeholder="placeholder">
                        <span class="input-group-append">
                            <span class="input-group-text">Appended Text</span>
                        </span>
                    </div>
                </div>
            </div>
            <!--Appended Text End-->

            <!--Button Drop Down Start-->
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Right button dropdown</label>
                <div class="col-lg-10">
                    <div class="input-group">
                        <input type="text" name="input2" class="form-control" placeholder="Right dropdown">
                        <div class="input-group-append">
                            <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown">Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="#" class="dropdown-item">Action</a>
                                <a href="#" class="dropdown-item">Another action</a>
                                <a href="#" class="dropdown-item">Something else here</a>
                                <a href="#" class="dropdown-item">One more line</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Button Drop Down End-->

            <!-- Text Area start -->
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Textarea</label>
                <div class="col-lg-10">
                    <textarea rows="3" cols="3" name="textarea" class="form-control"
                              placeholder="Default textarea"></textarea>
                </div>
            </div>

            <!-- Text Area End -->

            <!-- Prepended Checkbox-->
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Default checkbox</label>
                <div class="col-lg-10">
                    <div class="input-group">
											<span class="input-group-prepend">
												<span class="input-group-text">
													<input type="checkbox" checked>
												</span>
											</span>
                        <input type="text" name="checkinput" class="form-control" placeholder="Default checkbox addon">
                    </div>
                </div>
            </div>
            <!-- Prepended Checkbox -->
            <div class="d-flex justify-content-start align-items-center">
                <button type="reset" class="btn btn-light" id="reset">Reset</button>
                <button type="submit" class="btn btn-primary ml-3">Submit</button>
            </div>
        </form>
    </div>

@endsection
