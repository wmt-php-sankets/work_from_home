<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!--Global css -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap_limitless.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/colors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/components.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/layout.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/icons/icomoon/styles.min.css')}}">
    <!--Global css  end-->
    <!--Global js -->
    <script src="{{asset('assets/js/main/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/main/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <!--Global js End -->
    @yield('js')
</head>
<body>
@yield('form')
</body>
</html>
