<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
{{--        <script--}}
{{--            src="https://code.jquery.com/jquery-3.4.1.min.js"--}}
{{--            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="--}}
{{--            crossorigin="anonymous"></script>--}}
    </head>
    <body>
    <script src="{{asset('jquery.js')}}"></script>

        <div class="container-fluid">
{{--            {{ dd($countries) }}--}}
            <h1>Drop Down Select</h1>
            <label for="country">Select Countries</label>
            <select name="country" id="country" class="form-control">
                <option value="">Select Country</option>

            @foreach($countries as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>

    <div class="container-fluid">
        <label for="state">Select State</label>
        <select name="state" id="country" class="form-control">
            <option value="">State</option>
        </select>
    </div>
    <script>
        $(document).ready(function(){
            $('select[name="country"]').on('change',function(){
                var country_id = $(this).val();
                    if(country_id){
                        $.ajax({
                            url:'/getstates/'+country_id,
                            type:'GET',
                            dataType:'json',
                            success:function (data) {
                                    console.log(data);
                            }

                        });
                    }
            });
        });
    </script>
    </body>
</html>
