<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::resource('/form','FormController');
//Route::get('/master', function () {
//    return view('master');
//});
Route::get('/','MainController@index');
Route::get('/getstates/{id}','MainController@getstates');

